<?php
/**
 * @file
 * This module provides integration between the dcl_importer module and the
 * invite module
 */

/**
 * Implementation of hook_menu().
 */
function dcl_importer_invite_menu() {
  $items = array();

  // Create tabs on the invite page
  $items['invite/invite'] = array(
    'title' => t('Invite'),
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -1,
  );

  $items['invite/import'] = array(
    'title' => t('Import Contacts'),
    'description' => t('Import your email contacts'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('dcl_importer_form'),
    'access arguments' => array('access DCL Importer'),
    'type' => MENU_LOCAL_TASK,
  );

  $items['invite/import_select'] = array(
    'title' => t('Select Emails'),
    'description' => t('Import your email contacts'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('dcl_importer_form'),
    'access arguments' => array('access DCL Importer'),
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Implementation of hook_form_alter().
 */
function dcl_importer_invite_form_alter(&$form, $form_state, $form_id) {
  if ($form_id == "invite_form") {
    // Add the imported contacts to the invite form "To" field
    if (isset($_SESSION['dcl_importer_contacts'])) {
      $contacts = $_SESSION['dcl_importer_contacts'];
      $contacts = implode("\n", (array)unserialize($contacts));
      unset($_SESSION['dcl_importer_contacts']);
      if (!empty($contacts)) {
        $form['email']['#default_value'] = $contacts;
      }
    }
  }
}

/**
 * Implementation of hook_import_action().
 */
function dcl_importer_invite_import_action($op, &$contacts1, &$contacts2 = NULL) {

  switch ($op) {
    case 'form':
      if (!empty($contacts1)) {

        $contacts = array();
        foreach ($contacts1 as $contact) {
          $contacts[$contact['mail']] = $contact['name'] . " (" . $contact['mail'] . ")";
        }

        $form['dcl_importer']['contacts'] = array(
          '#type' => 'checkboxes',
          '#title' => t('Contacts'),
          '#default_value' => array_keys($contacts),
          '#options' => $contacts,
          '#description' => t("Select the contacts you'd like to invite."),
        );
      }

      $form['dcl_importer']['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Invite selected contacts'),
      );
      return $form;

    case 'submit':
      $_SESSION['dcl_importer_contacts'] = serialize($contacts1);
      $contacts2['redirect'] = 'invite';
      unset($contacts2['storage']);
      break;
  }
}
