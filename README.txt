
-- SUMMARY --

The dcl_importer lets users import contacts from various third party services
such as facebook, myspace, gmail, yahoo, friendfeed, twitter, and numerous
other services using the OpenInviter library. Currently the only functioning
services are those that return a valid email address (facebook, for example,
only returns the user id).

This module is currently under initial development and likely won't be
working 100%.

-- REQUIREMENTS --

 * The OpenInviter package from http://openinviter.com/download.php.
   Use the "general" version, not the drupal specific version. It should be
   uncompressed within the dcl_importer module directory:
      ex: sites/all/modules/dcl_importer/OpenInviter

 * For the OpenInviter to function properly you will need to have PHP5 installed
   with DOMDocument support and either cURL or WGET.

-- INSTALLATION --

 * Install as usual, see http://drupal.org/node/70151 for further information.

 * Run the script OpenInviter/postinstall.php to verify that everything is
   setup properly. This sets up the OpenInviter/config.php file.

 * Remove the file postinstall.php
 
 * You'll want to setup permissions appropriately (admin/user/permissions)

 * Browse to the settings page (admin/settings/dcl_importer) and configure
   your preferences there.

-- INVITE INTEGRATION --

You'll also need to install the invite module (http://drupal.org/project/invite) if you wish to have support for inviting people directly from the dcl imorter.

Once you've installed the invite module browse to the /invite page on your site and there will be an "Import Contacts" tab (or just go to /invite/dcl_import).

-- TESTING --

The only plugins tested and verified working so far are:
 * gmail
 
-- CONTACT --

For bug reports, feature suggestions and latest developments visit the
project page: http://drupal.org/project/dcl_importer

Current maintainer:
 * Scott Hadfield (hadsie) <hadsie@gmail.com>
