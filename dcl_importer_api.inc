<?php
/**
 *
 * @file
 * Contains the DCL Importer API calls
 *
 */

function dcl_importer_authenticate($provider, $username, $password) {
  include_once('OpenInviter/openinviter.php');
  $inviter = new OpenInviter();
  $inviter->startPlugin($provider);
  $internal = $inviter->getInternalError();

  $errors = array();
  if ($internal) {
    $errors['inviter'] = $internal;
  }
  elseif (!$inviter->login($username, $password)) {
    $internal = $inviter->getInternalError();
    $errors['login'] = $internal ? $internal : t('Login failed. Please check the email and password you have provided and try again later');
  }

  return array($inviter, $errors);
}

function dcl_importer_get_contacts($inviter) {
  $contacts = $inviter->getMyContacts();
  if ($contacts === FALSE) {
    drupal_set_message(t('Unable to get contacts.'), 'error');
  }
  $inviter->stopPlugin(TRUE);

  // Cleanup the results
  $contact_list = array();
  foreach ($contacts as $email => $name) {
    $email = trim($email);
    if ($email) {
      $contact_list[] = array(
        'mail' => $email,
        'name' => $name,
      );
    }
  }

  return $contact_list;
}

/**
 * Get a list of providers
 *
 * @param $all - If TRUE then returns all of the possible providers, if FALSE
 *   then returns only those providers that have been selected in the settings
 *   page.
 * @return
 *   an array containing available plugins in the form of
 *   (plugin_type, plugin_name) pairs, for example:
 *     Array(('gmail', 'GMail'), ('msn', 'MSN'))
 *
 */
function dcl_importer_get_providers($all = FALSE) {
  include_once('OpenInviter/openinviter.php');
  $inviter = new OpenInviter();

  $plugin_settings = variable_get('openinviter_plugins', NULL);
  $plugin_arr = array();
  foreach ($inviter->getPlugins() as $plugin_type => $plugins) {
    foreach ($plugins as $plugin => $plugin_details) {
      if ($all ||
        ($plugin_settings == NULL || $plugin_settings[$plugin])) {
        $plugin_arr[$plugin] = $plugin_details['name'];
      }
    }
  }
  return $plugin_arr;
}

/**
 * @return a list with two values:
 *  1. new contacts
 *  2. contacts that exist in the user table
 * 
 * The two arrays returned are of the same format as get_contacts;
 */
function dcl_importer_split_users($contacts) {
  if (empty($contacts)) {
    return $contacts;
  }

  $emails = array();
  foreach ($contacts as $contact) {
    $emails[] = $contact['mail'];
  }
  
  $query = db_query(
    'SELECT uid, name, mail FROM {users} WHERE mail in (' . implode(', ', array_fill(0, count($emails), "'%s'")) . ')',
    $emails);

  $exists = array();
  $emails = array();
  while ($user = db_fetch_object($query)) {
    $exists[$user->uid] = array(
      'mail' => $user->mail,
      'name' => $user->name,
    );
    $emails[] = $user->mail;
  }

  // Remove existing emails from the original $contacts array
  $new = array();
  foreach ($contacts as $contact) {
    if (!in_array($contact['mail'], $emails)) {
      $new[] = $contact;
    }
  }
  
  return array($new, $exists);
}
